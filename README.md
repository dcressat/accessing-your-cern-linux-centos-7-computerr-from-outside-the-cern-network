### Accessing Your CERN Linux Centos 7 Computer from From Outside The CERN Network, using XRDP.
<aside>  💡 
 Disclaimer 1.
 This documentation and methode of connecting to your CERN Linux Desktop computer, located as CERN is NOT officially supported
 </aside>
 <br>
 <aside>  💡
Disclaimer 2.
Wouldn't it be easier to tunnel the connection through CERNGT [using the Remote Desktop Connection Access facility](https://remotedesktop.web.cern.ch/remotedesktop/RDGateway/SetRemoteAccessAccounts.aspx), rather than using an ssh command line to tunnel to your CERN Linux computer, through lxplus?
Yes but if the name of your CERN Linux computer is more than 15 character long, it will not work.
</aside>
<br><br>
Domimique Cressatti Version 1.0
<br><br>
1. [Configuring Your_CERN_Linux_Centos7_Computer to be accessible from Home or from other locations outside CERN](#configuring-your-cern-linux-centos7-computer)
<br>
2. [Configuring yourRemmina Windows computer to access Your_CERN_Linux_Centos7_Computer inside the CERN network](#configuring-your-windows-computer-to-access-your-cern-linux-centos7-computer-inside-the-cern-network)
<br>
3. [Configuring your Linux computer to access Your_CERN_Linux_Centos7_Computer located inside the CERN network](#configuring-your-linux-computer-to-access-your-cern-linux-centos7-computer-located-inside-the-cern-network)
<br>
4. [Configuring your Mac to access Your_CERN_Linux_Centos7_Computer located inside the CERN network](#configuring-your-mac-to-access-your-cern-linux-centos7-computer-located-inside-the-cern-network)
<br>
5. [Troubleshooting and trying it](#troubleshooting-and-trying-it)
<br>
6. [How to configure multiple connections so you can concurently connect to different instances of Your_CERN_Linux_Centos7_Computers at the same time](#how-to-configure-multiple-connections-so-you-can-concurently-connect-to-different-instances-of-your-cern-linux-centos7-computers-at-the-same-time)
<br>
 
---
<aside><aside>

 💡 This tutorial will be based on a CERN Centos 7 openstack VM to ensure that all the bare necessary software will be installed to successfully connect to it and to run a full Gnome Desktop Environment.
</aside>

---
To configure Your_CERN_Linux_Centos7_Computer to access it from outside the CERN network computer, execute on your CERN  computer the following command as root:
<br>

### Configuring Your CERN Linux Centos7 Computer
```
curl -L https://gitlab.cern.ch/dcressat/accessing-your-cern-linux-centos-7-computerr-from-outside-the-cern-network/-/raw/master/Setting_Up_The_XRDP_Service.sh?inline=false -o /tmp/Setting_Up_The_XRDP_Service.sh && chmod 755 /tmp/Setting_Up_The_XRDP_Service.sh && /tmp/Setting_Up_The_XRDP_Service.sh 2>&1 |tee /tmp/Setting_Up_The_XRDP_Service.log
```

Now let’s check that we have configured everything correctly by checking if we can reach the XRDP service on Your_CERN_Linux_Centos7_Computer using telnet.
by executing the following commmand from lxplus:

```
telnet The_Name_Of_Your_CERN_Linux_Centos7_Computer 3389
```

If everything is working properly you should get something similar to the following:

```
Trying IP_Address_Of_Your_CERN_Linux_Computer
Connected to The_Name_Of_Your_CERN_Linux_Centos7_Computer
Escape character is '^]'.
^]
telnet> quit
```

Having confirmed that you can successfully connect to the XRDP service Of_Your_CERN_Linux_Computer, you can simultaneously press on  Ctrl + ]
then type “quit” and enter to end testing that you connect to the XRDP service of Your_CERN_Linux_Centos7_Computer.

---
### Configuring Your Windows Computer To Access Your CERN Linux Centos7 Computer Inside The CERN Network

Download and install Putty which should provide the command-line connection tool Plink which is a command-line connection tool similar to UNIX `ssh`.  To use it open a command line window, by typing `cmd.exe` in the Windows search bar and connect to lxplus using the following syntax:

```
plink.exe -P 21 -L 13389:The_Name_Of_Your_CERN_Linux_Computer:3389 Your_CERN_User_Name@lxplus.cern.ch
```

Once you are connected to lxplus having used the above syntax, all you need is to start a RDP (Microsoft Remote Desktop) session with the following connection details:

- Computer name: localhost:13389
- User Name: Your_CERN_User_Name

Like in the example below:

![RDPScreen1](/img/RDPScreen1.png)

You will then receive the following security warning on which you can safely press OK.

![RDPScreen2](/img/RDPScreen2.png)

And Finally the following logging dialog asking you for your CERN password before you are allowed to log on to your Gnome Desktop.

![RDPScreen3](/img/RDPScreen3.png)

---
### Configuring Your Linux Computer To Access Your CERN Linux Centos7 Computer Located Inside The CERN Network

To configure your Linux computer I will recommend that install use the Remmina package.

If you are using a Centos Based version of Linux execute the following command in a terminal window:

```
sudo yum -y install nux-dextop-release && yum -y install remmina-plugins-rdp.x86_64
```

If instead you are using a Debian/Ubuntu based version of Linux execute the following command in a terminal window:

```
sudo apt-get install remmina-plugin-rdp
```

Once you have installed the Remmina package, connect to lxplus using the following syntax:

```
ssh -L 3389:The_Name_Of_Your_CERN_Linux_Computer:3389 Your_CERN_User_Name@lxplus.cern.ch
```

Once you are connected to lxplus having used the above syntax, all you need is to start a Remmina session with the following connection details:

- Name: Whatever is meaningful to you
- Protocol: RDP
- Server: localhost:3389
- Username: Your_CERN_User_Name
- Password: Your_CERN_Password

Here is an example below. Save it and click on the “Connect” button.

![RemoteConnectionUsingRemminapng](img/RemminaScreenshot.png)

---
### Configuring Your Mac To Access Your CERN Linux Centos7 Computer Located Inside The CERN Network 
If you don't already have it you may obtain and install the Mac Remote Desktop client from [https://apps.apple.com/us/app/microsoft-remote-desktop/id1295203466?mt=12](https://apps.apple.com/us/app/microsoft-remote-desktop/id1295203466?mt=12)

Once you have installed the Mac RDP client, connect to lxplus using the following syntax:

```
ssh -L 3389:The_Name_Of_Your_CERN_Linux_Computer:3389 Your_CERN_User_Name@lxplus.cern.ch
```
Once you are connected to lxplus having used the above syntax, all you need is to start a Mac RDP session with the following connection details:

- Name: The_Name_Of_Your_CERN_Linux_Computer
- Protocol: RDP
- Server: localhost:3389
- Username: Your_CERN_User_Name
- Password: Your_CERN_Password

Here is an example below. Save it and click on the “Connect” button.

![RemoteConnectionUsingRemminapng](img/RemoteDesktopClientMac.png)

---
### Troubleshooting and trying it
You can check to see if the problem is related to your Linux computer inside the CERN by trying to connect to the following working Linux computer inside the CERN Network:
- Name: DC-CC7-Test02-VM
- Protocol: RDP
- Server: localhost:3389
- Username: Your_CERN_User_Name
- Password: Your_CERN_Password

If your are using Windows 1st establish plink connection with the following syntax:
```
plink.exe -P 21 -L 13389:DC-CC7-Test02-VM:3389:3389 Your_CERN_User_Name@lxplus.cern.ch`and then from Windows RDP connect to:`localhost:3389
```
and then with Windows Remote Desktop to connect to: `localhost:3389`, as instructed in the section [Configuring your Windows computer to access Your_CERN_Linux_Centos7_Computer inside the CERN network](#configuring-your-windows-computer-to-access-your-cern-linux-centos7-computer-inside-the-cern-network)

If your are using Linux 1st establish an ssh connection with the following syntax:
```
ssh -L 3389:DC-CC7-Test02-VM:3389 Your_CERN_User_Name@lxplus.cern.ch
```
and then from Remmina connect to: `localhost:3389`, as instructed in the section [Configuring your Linux computer to access Your_CERN_Linux_Centos7_Computer located inside the CERN network](#configuring-your-linux-computer-to-access-your-cern-linux-centos7-computer-located-inside-the-cern-network)

If your are using a Mac 1st establish an ssh connection with the following syntax:
```
ssh -L 3389:DC-CC7-Test02-VM:3389 Your_CERN_User_Name@lxplus.cern.ch
```
and then from Mac Remote Desktop client connect to: `localhost:3389`, as instructed in the section[Configuring your Mac to access Your_CERN_Linux_Centos7_Computer located inside the CERN network](#configuring-your-mac-to-access-your-cern-linux-centos7-computer-located-inside-the-cern-network)

If the connection to DC-CC7-Test02-VM works run the following checks on Your_CERN_Linux_Centos7_Computer.
* As root on Your_CERN_Linux_Centos7_Computer execute:`netstat -antp |grep 3389` which should return:
```
tcp6       0      0 :::3389                 :::*                    LISTEN      1643/xrdp  :
```
If you don't get any result it means that XRDP service is not running. Try run it by executing: `systemctl start xrdp` and check again if it is running by executing the above `netstat -antp |grep 3389` command. If that works check that the service enabled to start by default by executing: `systemctl enable xrdp`. 
If none of the above you will have to diagnose the problem by looking in /var/log/messsage by executing: `less /var/log/messsage` and look for any error related to xrdp

Now if the xrpd is running it could be that likely that Your_CERN_Linux_Centos7_Computer firewall is not allowing connections on the port 3389 which if you execute: `iptables -S |grep 3389` should return
```
 [root@dc-cc7-test02-vm ~]# iptables -S |grep 3389
-A IN_public_allow -p tcp -m tcp --dport 3389 -m conntrack --ctstate NEW,UNTRACKED -j ACCEPT
```

Beyond that, you may open a ticket with the 2nd level Linux support team who MAY help

---
### How to configure multiple connections so you can concurently connect to different instances of your cern linux centos7 computers at the same time

I want to access more than 1 of my CERN Linux Centos 7 at the same time
The 1st connection and way to connect to your Your_1st_CERN_Linux_Computer will remain the same:
```
ssh -L 3390:The_Name_Of_Your_1st_CERN_Linux_Computer:3389 Your_CERN_User_Name@lxplus.cern.ch
```

**But** on your Your_2nd_CERN_Linux_Computer, you will have to change the port over which the RDP service listens by default `3389` to something else. To do it safely we will check with telnet (like we did it before) which port is in use. So both on your on your_2nd_CERN_Linux_Computer and home computer you execute the following command with the following syntax specifying a port not in use (Ex:3391): 
``` 
telnet The_Name_Of_Your_2nd_CERN_Linux_Centos7_Computer 3391
telnet The_Name_Of_Your_Home_Computer 3391
```
If you don't get replies from either of them you're good to go to change default port (3389) over which XRDP service licences by modifying the following line in `/etc/xrdp/xrdp.ini`:

```
port=3389
```
To: 
```
port=3391
```
BTW you can also quickly make this change using the following sed command with the following syntax (assuming that you have found port 3391 wasn't being used neither on Your_2nd_CERN_Linux_Computer and your Home computer):
```
sed -i "s/port=3389/port=tcp:\/\/:3391/" /etc/xrdp/xrdp.ini
```

* Then amend the firewall directive to:
```
firewall-cmd --permanent --add-port=3391/tcp && firewall-cmd --reload
chcon --type=bin_t /usr/sbin/xrdp && chcon --type=bin_t /usr/sbin/xrdp-sesman
```
* Then restart the XRDP service to make the changes effective:
```
systemctl restart xrdp
```
Following this you will have to m change the new to connect to your other instance such as:
- Name: The_Name_Of_Your_2nd_CERN_Linux_Computer
- Protocol: RDP
- Server: localhost:3391
- Username: Your_CERN_User_Name
- Password: Your_CERN_Password

So effectively in your other instance all you will need is to change the port number after `ssh -L` in the connection through lxplus.

#### For example here are 2 test VMs on which you can test concurrent connections.
* dc-cc7-test02vm which you will have to connect with the following syntax:
``` 
ssh -L 3389:dc-cc7-test02-vm:3389 Your_CERN_User_Name@plus.cern.ch
```
* dc-cc7-test03-vm which you will have to connect with the following syntax:
``` 
ssh -L 3391:dc-cc7-test03-vm:3391 Your_CERN_User_Name@lxplus.cern.ch
```
