#!/bin/bash

# A Quick trick to add date and time stamp so we when can find out when root ran what root history making it easier to debug issues.
echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> /root/.bash_profile

echo "============================="
echo "installing if needed the Gnome Desktop Environment and X11 Server"
yum install -y xorg-x11-server-Xorg xorg-x11-xauth xorg-x11-apps && yum -y groupinstall "Software Development Workstation (CERN Recommended Setup)" && yum -y groupinstall "GNOME Desktop" "Graphical Administration Tools"

echo "============================="
echo "Installing the XRDP service which will allow to connect to your Gnome Desktop Environment"
yum -y install xrdp tigervnc-server

echo "============================="
echo "making sure that your CERN Centos 7 computer will run in Graphical mode by default"
ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target

echo "============================="
echo "Opening the firewall ports for the XRDP service to function properly"
firewall-cmd --permanent --add-port=3389/tcp && firewall-cmd --reload
chcon --type=bin_t /usr/sbin/xrdp && chcon --type=bin_t /usr/sbin/xrdp-sesman
# keep authentication file in /tmp/$USER
sed -i '/\[Globals\]/a AuthFilePath=\/tmp\/%s\/xrdp-vnc-passwd' /etc/xrdp/sesman.ini

#Let's make sure that we're in runlevel5 before starting XRDP
init 5
echo "============================="
echo "Enabling and starting the XRDP service"
systemctl enable xrdp && systemctl start xrdp
exit
